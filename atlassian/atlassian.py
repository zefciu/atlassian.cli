import argparse
import configparser
import logging
import os
import restkit
import textwrap


log = logging.getLogger(__name__)


class Atlassian(restkit.Resource):
    """Abstract restkit resource and endhiran plugin for atlassian services."""

    def __init__(self, settings):

        class DefaultHeaders(object):
            """Restkit filter for default headers."""

            def on_request(self, request):
                
                request.headers.update({
                    "X-Atlassian-Token": "nocheck",
                    "Accept": "application/json",
                    # "Content-Type": "application/x-www-form-urlencoded",
                })

        headers = DefaultHeaders()
        self.auth = restkit.BasicAuth(self.username, self.password)
        super(Atlassian, self).__init__(self.host, filters = [
            self.auth,
            headers
        ])

    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser(
            description=textwrap.dedent(cls.__doc__)
        )
        parser.add_argument(
            '-n',
            '--project-name',
            help='The name for the project',
            required=True,
        )
        parser.add_argument(
            '-k',
            '--project_key',
            help='The key for the project',
            required=True,
        )
        parser.add_argument(
            '-u',
            '--username',
            help='The username for the project owner',
            required=True,
        )
        return parser

    @classmethod
    def cli(cls):
        """The cli entry point."""
        config = configparser.ConfigParser()
        config.read(os.path.expanduser('~/.atlassian.cli'))
        parser = cls.get_parser()
        args = parser.parse_args()
        self = cls(config['DEFAULT'])
        result = self.project_create(
            args.username,
            args.project_name,
            args.project_key
        )
