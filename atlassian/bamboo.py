import json
import logging
import re
import restkit
import urllib

from atlassian import Atlassian


log = logging.getLogger(__name__)


class Bamboo(Atlassian):
    """Create a project in Bamboo."""

    name = 'Bamboo'

    def __init__(self, settings):
        self.host = settings['atlassian.bamboo_host']
        self.username = settings['atlassian.bamboo_username']
        self.password = settings['atlassian.bamboo_password']
        super(Bamboo, self).__init__(settings)

    def project_create(self, user_name, project_name, project_key):
        log.debug("Creating project %s" % project_name)
        url = '/build/admin/create/createPlan.action'
        # Heavy reverse engineering below. Don't ask "why". 
        payload = urllib.urlencode([
            ('existingProjectKey', 'newProject'),
            ('selectFields', 'existingProjectKey'),
            ('selectFields', 'selectedRepository'),
            ('selectFields', 'repository.cvs.authType'),
            ('selectFields', 'repository.cvs.selectedVersionType'),
            ('selectFields', 'repository.svn.authType'),
            ('selectFields', 'repository.hg.authentication'),
            ('selectFields', 'repository.bitbucket.repository'),
            ('selectFields', 'repository.bitbucket.branch'),
            ('selectFields', 'repository.git.authenticationType'),
            ('selectFields', 'repository.github.repository'),
            ('selectFields', 'repository.github.branch'),
            ('selectFields', 'selectedBuildStrategy'),
            ('projectName', project_name),
            ('projectKey', project_key),
            ('chainName', 'Plan ' + project_name),
            ('chainKey', project_key + '1'),
            ('chainDescription', ''),
            ('selectedRepository', 'nullRepository'),
            ('selectFields', 'selectedRepository'),
            ('selectedBuildStrategy', 'schedule'),
            ('selectFields', 'selectedBuildStrategy'),
            ('repository.change.poll.type', 'PERIOD'),
            ('repository.change.poll.poll.pollingPeriod', '180'),
            ('repository.change.poll.cronExpression', '0 0 0 ? * *'),
            ('repository.change.schedule.cronExpression', '0 0 0 ? * *'),
            ('save', 'Configure Tasks'),
            ('checkBoxFields','repository.p4.manageWorkspace'),
            ('checkBoxFields','repository.hg.ssh.compression'),
            ('checkBoxFields','repository.bitbucket.git.useShallowClones'),
            ('checkBoxFields','repository.git.useShallowClones'),
            ('checkBoxFields','repository.github.useShallowClones'),
            ('repository.bitbucket.password.change', 'true'),
            ('repository.cvs.authType', 'password'),
            ('repository.cvs.quietPeriod', '0'),
            ('repository.cvs.selectedVersionType', 'head'),
            ('repository.git.authenticationType', 'NONE'),
            ('repository.git.useShallowClones', 'true'),
            ('repository.github.useShallowClones', 'true'),
            ('repository.hg.authentication', 'PASSWORD'),
            ('repository.svn.authType', 'password'),
            ('selectedWebRepositoryViewer', 'bamboo.webrepositoryviewer.provided:noRepositoryViewer'),
            ('temporary.cvs.passphraseChange', 'true'),
            ('temporary.cvs.passwordChange', 'true'),
            ('temporary.git.password.change', 'true'),
            ('temporary.git.ssh.key.change', 'true'),
            ('temporary.git.ssh.passphrase.change', 'true'),
            ('temporary.github.password.change', 'true'),
            ('temporary.hg.password.change', 'true'),
            ('temporary.hg.ssh.key.change', 'true'),
            ('temporary.hg.ssh.passphrase.change', 'true'),
            ('temporary.p4.passwordChange', 'true'),
            ('temporary.svn.passphraseChange', 'true'),
            ('temporary.svn.passwordChange', 'true'),
            ('temporary.svn.sslPassphraseChange', 'true')
        ])
        resp = self.post(url, payload, headers=[
            ('Content-Type', 'application/x-www-form-urlencoded',),
        ]).body_stream()
        for i in range(2, 4):
            url = ('/rest/api/latest/clone/{project_key}-{build_key}:'
                '{to_project_key}-{to_build_key}'.format(
                    project_key=project_key,
                    build_key=(project_key + '1'),
                    to_project_key=project_key,
                    to_build_key=(project_key + str(i)),
            ))
            self.put(url)
        return

    def project_exists(self, name):
        log.info("Checking whether project '{0}' exists".format(name))
        path = "/rest/api/latest/project/{0}".format(name)
        status = self.api_status(path).status
        if status == 200:
            return True
        else:
            return False

    def run(self, data):
        self.project_create(
            data['user_name'],
            data['project_name'],
            data['project_key']
        )
        url = '{0}/project/viewProject.action?projectKey={1}'.format(
            self.host,
            data['project_key'],
        )
        return (True, 'Created project <a href="{url}">{url}</a>'.format(
            url=url
        ))

    def test(self, data):
        if len(data['project_key']) > 9:
            return """Project key must not be longer that 10 characters."""
        try:
            self.get('/rest/api/latest/project/{}'.format(data['project_key']))
        except restkit.errors.ResourceNotFound:
            pass
        else:
            return 'Project with this key already exists'
        return True

    def key_exists(self, project, key):
        log.info("Checking whether plan '{0}' contains key '{1}'".format(project, key))
        path = "/rest/api/latest/plan/{0}-{1}".format(project, key)
        status = self.api_status(path).status
        if status == 200:
            return True
        else:
            return False
