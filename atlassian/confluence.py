"""Confluence utilities."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import bs4
import Cookie
import json
import logging
import restkit
import urllib

from .atlassian import Atlassian


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


class Confluence(Atlassian):
    """Create a project in Confluence."""

    name='Confluence'

    def __init__(self, settings):
        self.host = settings['atlassian.confluence_host']
        self.username = settings['atlassian.confluence_username']
        self.password = settings['atlassian.confluence_password']

        super(Confluence, self).__init__(settings)

    def project_create(self, user_name, project_name, project_key):
        log.debug("Creating project %s" % project_name)
        result = self.get('/dashboard.action')
        soup = bs4.BeautifulSoup(result.body_string())
        cookie = Cookie.SimpleCookie()
        cookie.load(result.headers['Set-Cookie'])
        def find_token_meta(tag):
            return (
                tag.name == 'meta' and
                tag.attrs.get('id') == 'atlassian-token'
            )
        meta_tag = soup.find_all(find_token_meta)[0]
        token = meta_tag.attrs['content']
        url = "/spaces/createspace.action"
        payload = urllib.urlencode({
            'atl_token': token,
            'key': project_key,
            "name": project_name,
        })
        self.post(url, payload, headers=[
            ('Content-Type', 'application/x-www-form-urlencoded',),
            ('Cookie', 'JSESSIONID={}'.format(cookie['JSESSIONID'].value),),
        ])

    def test(self, data):
        try:
            self.get('/rest/prototype/1/space/{}'.format(data['project_key']))
        except restkit.errors.ResourceNotFound:
            pass
        else:
            return 'Space with this key already exists'
        return True

    def run(self, data):
        try:
            self.project_create(
                data['user_name'],
                data['project_name'],
                data['project_key'],
            )
        except BaseException as exc:
            
            exc_info = repr(exc)
            if hasattr(exc, 'message'):
                exc_info = '\n'.join([exc_info, exc.message])
            return False, exc_info
        url = self.host + '/display/' + data['project_key']
        
        return True, """Created space '<a href="{0}">{1}</a>'""".format(
            url, url,
        ),
