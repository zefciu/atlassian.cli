import logging
from atlassian import Atlassian


log = logging.getLogger(__name__)


class Fisheye(Atlassian):

    def _api_gwt_call(self, action, name, payload=""):
        if not payload:
            payload = """7|0|6
                https://%(host)s/static/mesrp6/gwt/AdminGwtModule/
                CF9583951445BD6FCD6F31E945692031
                com.atlassian.fecru.gwt.admin.client.RepositoryAdminRpcService
                %(action)s
                java.lang.String/2004016611
                %(name)s
                1|2|3|4|1|5|6|""" % {"host": self.host, "action": action, "name": name}
        headers = {"Content-type": "text/x-gwt-rpc"}
        path = "/gwt/RepositoryAdminRpcService"
        payload = payload.replace("\n", "|").replace(" ", "")
        return self.api(path, "POST", payload, headers)

    def get_repositories(self):
        url = "/rest/api/1.0/rest-service-fe/repositories-v1"
        repos = self.api(url)
        return repos["repository"]

    def project_repositories(self, prefix):
        repositories = self.get_repositories()
        return [repo for repo in repositories if repo["name"].startswith(prefix)]

    def project_repositories_names(self, prefix):
        return [x["name"] for x in self.project_repositories(prefix)]

    def project_delete(self, prefix):
        log.warn("Deleting repositories prefixed with: '%s'" % prefix)
        repos = self.project_repositories_names(prefix)
        for repo in repos:
            self.indexing_delete(repo)

    def project_disable(self, prefix):
        log.info("Disabling project %s" % prefix)
        for repo in self.project_repositories_names(prefix):
            self.indexing_disable(repo)

    def project_stop(self, prefix):
        log.info("Stopping project %s" % prefix)
        for repo in self.project_repositories_names(prefix):
            self.indexing_stop(repo)

    def repository_start(self, name):
        log.info("Starting repository %s" % name)
        return self._api_gwt_call("startRepository", name)

    def repository_stop(self, name):
        log.info("Stopping repository %s" % name)
        return self._api_gwt_call("stopRepository", name)

    def repository_delete(self, name):
        self.repository_stop(name)
        log.info("Deleting repository %s" % name)
        url = "/rest/api/1.0/rest-service-fe/managed-repositories-v1/git/%s" % name
        return self.api(url, method="DELETE")

    def indexing_stop(self, name):
        log.info("Stopping indexing %s" % name)
        return self._api_gwt_call("stopRepository", name)

    def indexing_disable(self, name):
        log.info("Disabling indexing %s" % name)
        payload = """7|0|7
            https://{host}/static/mesrp6/gwt/AdminGwtModule/
            CF9583951445BD6FCD6F31E945692031
            com.atlassian.fecru.gwt.admin.client.RepositoryAdminRpcService
            enableRepository
            java.lang.String/2004016611
            Z
            {name}
            1|2|3|4|2|5|6|7|0|""".format(name=name, host=self.host)
        return self._api_gwt_call("disableRepository", name, payload=payload)

    def indexing_enable(self, name):
        log.info("Enable indexing %s" % name)
        return self._api_gwt_call("enableRepository", name)

    def indexing_delete(self, name):
        self.indexing_stop(name)
        log.info("Deleting indexing %s" % name)
        return self._api_gwt_call("deleteRepository", name)

    def indexing_add(self, name, url, password, description):
        log.info("Adding to Fisheye indexing: %s as %s" % (url, name))
        payload = """7|0|15
            https://fisheye.office/static/mesrp6/gwt/AdminGwtModule/
            CF9583951445BD6FCD6F31E945692031
            com.atlassian.fecru.gwt.admin.client.RepositoryAdminRpcService
            createRepository
            com.atlassian.fecru.gwt.admin.shared.repositories.RepositoryDetails/923546390|Z
            com.atlassian.fecru.gwt.admin.shared.repositories.GitRepositoryDetails/969678029
            com.atlassian.fecru.gwt.admin.shared.repositories.AuthenticationOptions/462934901
            %(password)s
            com.atlassian.fecru.gwt.admin.shared.auth.AuthenticationStyle/1498033890
            %(url)s
            %(description)s
            com.atlassian.fecru.gwt.admin.shared.repositories.RepositoryKind/1701169051
            %(name)s
            |1|2|3|4|3|5|6|6|7|0|0|8|0|0|9|0|10|3|11|12|13|2|0|14|15|1|0|""" % {
            "password": password,
            "url": url,
            "name": name,
            "description": description}
        headers = {"Content-type": "text/x-gwt-rpc"}
        path = "/gwt/RepositoryAdminRpcService"
        payload = payload.replace("\n", "|").replace(" ", "")
        return self.api(path, "POST", payload, headers)
