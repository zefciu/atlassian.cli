"""JIRA utilities."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import json
import logging
import restkit
import urllib

from .atlassian import Atlassian


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


class Jira(Atlassian):
    """Create a project in JIRA."""

    name='JIRA'

    def __init__(self, settings):
        self.host = settings['atlassian.jira_host']
        self.username = settings['atlassian.jira_username']
        self.password = settings['atlassian.jira_password']

        super(Jira, self).__init__(settings)

    def project_create(self, user_name, project_name, project_key):
        log.debug("Creating project %s" % project_name)
        url = "/rest/project-templates/1.0/templates"
        payload = urllib.urlencode({
            "name": project_name,
            "key": project_key,
            "lead": user_name,
            "keyEdited": "true",
            "projectTemplateWebItemKey":
                "com.atlassian.jira-core-project-templates:jira-issuetracking-item",
            "projectTemplateModuleKey":
                "com.atlassian.jira-core-project-templates:jira-issuetracking-item",
        })

        return json.load(self.post(url, payload, headers=[
            ('Content-Type', 'application/x-www-form-urlencoded',),
        ]).body_stream())

    def test(self, data):
        # This is very ineffective as it requires to fetch all the projects
        # and search them on our side. If JIRA offers a way to actually
        # *search* projects by name, this can be changed
        if len(data['project_key']) > 10:
            return """Project key must not be longer that 10 characters."""
        try:
            self.get('/rest/api/2/user/', username=data['user_name'])
        except restkit.errors.ResourceNotFound:
            return 'Cannot find user'
        for project in json.load(
            self.get('/rest/api/2/project').body_stream()
        ):
            if project['name'] == data['project_name']:
                return 'Project with this name already exists'
            if project['key'] == data['project_key']:
                return 'Project with this key already exists'
        return True

    def run(self, data):
        try:
            response = self.project_create(
                data['user_name'],
                data['project_name'],
                data['project_key'],
            )
        except BaseException as exc:
            exc_info = repr(exc)
            if hasattr(exc, 'message'):
                exc_info = '\n'.join([exc_info, exc.message])
            return False, exc_info
        url = self.host + response['returnUrl']
        
        return True, """Created project '<a href="{0}">{1}</a>'""".format(
            url, data['project_name'],
        ),


jira_project_create = Jira.cli
