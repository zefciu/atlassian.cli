import logging
from atlassian import Atlassian


log = logging.getLogger(__name__)


class Stash(Atlassian):

    def get_repo_url(self, project, repo):
        return "https://%(username)s@%(host)s/scm/%(project)s/%(repo)s.git" % {
            "username": self.username,
            "host": self.host,
            "project": project,
            "repo": repo}

    def project_repositories(self, project):
        log.debug("Getting repositories list for %s" % project)
        url = "/rest/api/1.0/projects/%s/repos?limit=999" % project
        repos = self.api(url)
        return repos["values"]

    def project_repositories_names(self, project):
        return [x["name"] for x in self.project_repositories(project)]
