from setuptools import setup, find_packages

with open('entry_points.ini') as f:
    entry_points = f.read()

setup(
    name='atlassian.cli',
    version='0.1.2',
    packages=find_packages(),
    install_requires = ['restkit', 'configparser', 'beautifulsoup4'],
    entry_points=entry_points,
)
